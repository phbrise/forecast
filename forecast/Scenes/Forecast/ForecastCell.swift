//
//  ForecastCell.swift
//  forecast
//
//  Created by Phanvit Chevamongkolnimit on 30/1/19.
//  Copyright © 2019 Breeze. All rights reserved.
//

import UIKit

class ForecastCell: UICollectionViewCell {
    @IBOutlet weak var timeLbl:UILabel!
    @IBOutlet weak var tempLbl:UILabel!
    @IBOutlet weak var humidityLbl:UILabel!

}
