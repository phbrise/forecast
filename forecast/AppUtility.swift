//
//  AppUtil.swift
//  forecast
//
//  Created by Breeze on 23/1/2562 BE.
//  Copyright © 2562 Breeze. All rights reserved.
//

import Foundation


//api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=423b28a6633c8b55aed8d8a1be60614b

struct AppUrl {
    
    private struct Domains {
        static let Url = "https://api.openweathermap.org/"
    }
    
    private struct AppIds {
        static let Id = "423b28a6633c8b55aed8d8a1be60614b"
    }
    
    private  struct Routes {
        static let Api = "data/2.5/"
    }
    
    private  static let Domain = Domains.Url
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route
    
    static let AppId = AppIds.Id
    static let Header = ["Content-Type": "application/json; charset=utf-8"]
    
    static var Forecast: String {
        return BaseURL  + "forecast"
    }
    static var Weather: String {
        return BaseURL  + "weather"
    }
    
}
