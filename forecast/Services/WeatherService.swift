//
//  WeatherService.swift
//  forecast
//
//  Created by Phanvit Chevamongkolnimit on 23/1/19.
//  Copyright © 2019 Breeze. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherService:WeatherDataProtocal {
    
    func getWeather(lat:String, lon:String, units:String,completion:@escaping(WeatherData?,Error?) -> Void) {

        let url = AppUrl.Weather + "?lat=\(lat)&lon=\(lon)&appid=\(AppUrl.AppId)&units=\(units)"

        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: AppUrl.Header).responseJSON { (response) in
            var weather:WeatherData?
            var main:MainData!
            if response.result.error == nil {
                guard let data = response.data else { return }
                do {
                    let json = try JSON(data: data)
                    print("URL : \(url)\nWeather : \(json)")

                    
                    let name = json["name"].stringValue
                    let dt = json["dt"].intValue
                    let mainJs = json["main"]
                    let temp = mainJs["temp"].doubleValue
                    let humidity = mainJs["humidity"].intValue
                    main = MainData(temp:temp,humidity: humidity)
                    weather = WeatherData(main:main, dt:dt, name:name)
                    completion(weather,nil)
                } catch {
                    completion(weather,error)
                }
            } else {
                completion(weather,response.result.error)
            }
        }
    }
    
    func getForecast(lat:String, lon:String, units:String,completion:@escaping(ForecastData?,Error?) -> Void) {
        let url = AppUrl.Forecast + "?lat=\(lat)&lon=\(lon)&appid=\(AppUrl.AppId)&units=\(units)"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: AppUrl.Header).responseJSON { (response) in
            var forecast:ForecastData?
            var forecastLists:[MainList] = []
            if response.result.error == nil {
                guard let data = response.data else { return }
                do {
                    let json = try JSON(data: data)
                    print("URL : \(url)\nForecast : \(json)")
                    let city = json["city"]
                    let name = city["name"].stringValue
                    let lists = json["list"].array
                    for list in lists ?? [] {
                        let dt = list["dt"].intValue
                        let main = list["main"]
                        let temp = main["temp"].doubleValue
                        let humidity = main["humidity"].intValue
                        let listData = MainList.init(dt: dt, temp: temp, humidity: humidity)
                        forecastLists.append(listData)
                    }
                    forecast = ForecastData.init(name: name, main: forecastLists)
                    completion(forecast,nil)
                } catch {
                    completion(forecast,error)
                }
            } else {
                completion(forecast,response.result.error)
            }
        }

    }
    

}
