//
//  Weather.swift
//  forecast
//
//  Created by Phanvit Chevamongkolnimit on 23/1/19.
//  Copyright © 2019 Breeze. All rights reserved.
//

import Foundation

struct WeatherData {
    var main: MainData
    var dt: Int
    var name: String
}

struct ForecastData {
    var name: String
    var main: [MainList]
}

struct MainData {
    var temp: Double
    var humidity: Int
}

struct MainList {
    var dt: Int
    var temp: Double
    var humidity: Int
}
